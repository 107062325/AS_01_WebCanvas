# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       |          |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    本網頁小畫家採極簡風格，不做過多的裝飾，但功能還算是齊全。
    筆畫的部分使用網頁內建調色盤製作，並且有滾輪可調整筆畫粗細。
    文字的部分有三種字體、六種大小可選。
    支援圖片上傳及下載的功能。
    圖形繪畫目前僅支援三角形。
    若不小心畫錯，可用redo undo恢復。
    畫到一半想重劃，可使用clear清除畫布。

### Function description

    no your bouns function.

### Gitlab page link

    https://107062325.gitlab.io/AS_01_WebCanvas/
    
### Others (Optional)

    蠻有趣的作業，如果沒有時間壓力，我會好好做的，並且新增更多功能。

