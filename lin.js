var canvas = document.getElementById('mycanvas');
var context = canvas.getContext('2d');
var geo;
var state = context.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);

context.lineWidth = 1;
context.lineCap = "round";
var font_size;
var font_style;
var painting = false;
var erasing = false;
var redoing = false;
var undoing = false;
var tri_ing = false;
var doing = " ";
var x0, y0;
var imgdata;
//////////----------redo undo
window.addEventListener('popstate', changeStep, false);

function changeStep(e) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    if (e.state) {
        context.putImageData(e.state, 0, 0);
    }
}
////state change
function undo() {
    
    window.history.go(-1);
    box.style = "visibility:hidden";
}

function redo() {
    
    window.history.go(+1);
    box.style = "visibility:hidden";
}

function totext() {
    doing = "text";
}
function triangle()
{
    doing = "triangle";
    imgdata=context.getImageData(0, 0, canvas.width, canvas.height);
}

function toeraser() {
    doing = "eraser";
    box.style = "visibility:hidden";
}

function topencil() {
    doing = "pencil";
    box.style = "visibility:hidden";
}
/////////------clear all
function Myclear() {
    if (confirm("sure to clear?")) context.clearRect(0, 0, canvas.width, canvas.height);
}

/////----save file-----//////
function trigger_save() {
    document.getElementById("to_url").click();
}
//////////
/////---text control----////////
function changefontstyle(n) {
    font_style = n.value;
    context.font = font_size + " " + font_style;
}

function changefontsize(n) {
    font_size = n.value + 'px';
    context.font = font_size + " " + font_style;
}

function checkenter(e) {
    if (e.keyCode == 13) {
        context.fillText(box.value, parseInt(this.style.left, 10) - canvas.offsetLeft, parseInt(this.style.top, 10) - canvas.offsetTop + 20);
        box.value = "";
        box.style = "visibility:hidden";
        var state = context.getImageData(0, 0, canvas.width, canvas.height);
        window.history.pushState(state, null);
    }
}
//////////////////
///////-----mouse control------///////////
canvas.addEventListener('mousemove', function (e) {
    if (doing == "eraser") erases(e);
    else if (doing == "pencil") draw(e);
    else if (doing == "triangle") draw_tri(e);
    else return;
});

canvas.addEventListener('mousedown', function (e) {
    if (doing == "eraser") {
        erasing = true;
        erases(e);
    } else if (doing == "pencil") {
        painting = true;
        draw(e);
    } else if (doing == 'text') {
        box.style = "visibility:visible";
        var posx = e.pageX + 'px';
        var posy = e.pageY + 'px';
        box.style.left = posx;
        box.style.top = posy;
        box.onkeydown = checkenter;
    } else if (doing == "triangle") {
        imgdata=context.getImageData(0, 0, canvas.width, canvas.height);
        tri_ing = true;
        x0 = e.offsetX;
        y0 = e.offsetY;
    }
    else {}
});

canvas.addEventListener('mouseup', function (e) {
    painting = false;
    erasing = false;

    if (doing == "triangle") {
        tri_ing = false;
        context.beginPath();
        context.lineJoin = ctx.lineCap = 'round';
        context.lineWidth = document.getElementById("size").value;
        context.strokeStyle = document.getElementById("color").value;
        context.moveTo(x0, y0);
        context.lineTo((e.offsetX + x0) / 2, e.offsetY);
        context.lineTo(e.offsetX, y0);
        context.lineTo(x0, y0);
        context.stroke();
        
        var state = context.getImageData(0, 0, canvas.width, canvas.height);
        window.history.pushState(state, null);
    } else {
        context.beginPath();
        var state = context.getImageData(0, 0, canvas.width, canvas.height);
        if (doing == "text") return;
        window.history.pushState(state, null);
    }

});
////-------pencil and eraser---------////////
function draw(e) {
    if (!painting) return;
    xpos = e.clientX - canvas.offsetLeft;
    ypos = e.clientY - canvas.offsetTop;
    context.globalCompositeOperation = "source-over";

    context.strokeStyle = document.getElementById("color").value;

    context.lineWidth = document.getElementById("size").value;
    context.lineTo(xpos, ypos);
    context.stroke();
    context.beginPath();
    context.moveTo(xpos, ypos);
}

function erases(e) {
    if (!erasing) return;
    xpos = e.clientX - canvas.offsetLeft;
    ypos = e.clientY - canvas.offsetTop;
    context.globalCompositeOperation = "destination-out";

    context.strokeStyle = document.getElementById("color").value;

    context.lineWidth = document.getElementById("size").value;
    context.lineTo(xpos, ypos);
    context.stroke();
    context.beginPath();
    context.moveTo(xpos, ypos);
}
/////////////



///////------open file--------////////////
function trigger_open() {
    document.getElementById("file").click();
}

document.getElementById("file").addEventListener('change', function (e) {
    if (confirm("Do you want to clear the current work?")) context.clearRect(0, 0, canvas.width, canvas.height);
    var tmp_url = URL.createObjectURL(e.target.files[0]);
    var image = new Image();
    image.src = tmp_url;
    image.addEventListener("load", function () {
        image_w = image.naturalWidth;
        image_h = image.naturalHeight;
        new_w = image_w;
        new_h = image_h;
        img_r = image_w / image_h;
        canvas_r = canvas.width / canvas.height;
        if (image_w <= canvas.width && image_h <= canvas.height) context.drawImage(image, 0, 0);
        else if (img_r > canvas_r) context.drawImage(image, 0, 0, canvas.width, canvas.width / img_r);
        else if (img_r < canvas_r) context.drawImage(image, 0, 0, canvas.height * img_r, canvas.height);
        else context.drawImage(image, 0, 0, canvas.height, canvas.height);
        URL.revokeObjectURL(tmp_url);
    });
});
///////////////
function draw_tri(e) {
    
    if (tri_ing) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.beginPath();
        context.lineJoin = context.lineCap = 'round';
        context.lineWidth = document.getElementById("size").value;
        context.strokeStyle = document.getElementById("color").value;
        context.moveTo(x0, y0);
        context.lineTo((e.offsetX + x0) / 2, e.offsetY);
        context.lineTo(e.offsetX, y0);
        context.lineTo(x0, y0);
        context.putImageData(imgdata,0,0);
        context.stroke();
    }
}